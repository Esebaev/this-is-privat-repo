def print_list(array):
    for item in array:
        print item

def main():
    names = []

    names.append("Alisher")
    names.append("Argen")

    print_list(names)

    print len(names)

    names.remove("Alisher")

    print_list(names)
    print("-----")
    names.append("Tilek")
    print_list(names)