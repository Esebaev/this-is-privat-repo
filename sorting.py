from program import print_list

numbers = [34234,234,234,2,34,223,4 ,2342, 7,37]

# names = ["Alisher", "Tilek", "Sabina", "Meerim", "Bahhha"]
#
# d = sorted(d, reverse=True)
# print_list(d)
#
# print_list(sorted(names, reverse=True))


def my_sort(array):
    for i in range(len(array)):
        for j in range(i+1, len(array)):
            if array[j] < array[i]:
                array[i], array[j] = array[j], array[i]

    return array

print_list(my_sort(numbers))

